# 概述

这就是一个通用的服务发布Jar包的 **build.gradle** 文件。已经封装好了我们需要发布jar包的相关配置(包括发布到哪里，发布的版本、账号、密码等)。

# maven插件

由于**gradle**的机制，不能在导入的gradle文件中声明插件，所以你必须在宿主项目的**build.gradle**文件中声明`maven-publish`插件，如下：

``` gradle
// build.gradle
plugins {
    // other plugins...
    
    id 'maven-publish'
}
```

# 项目名称

你需要在自己项目的根目录下创建一个**settings.gradle**文件，并且设置项目名称，如下:

``` gradle
// settings.gradle

rootProject.name = 'project_name'
```

# 发布包名

在这个子**build.gradle**中，使用`project.group`定义的值作为发布时的**groupId**，所以开发者包含这个项目之后，需要在父**build.gradle**中明确设置`project.group`的值。例如:

```
project.group = 'com.example'
```

# 发布凭证

本项目可以让开发者指定特定的发布地址以及凭证。

设置变量`MVN_RELEASE_URL`和`MVN_SNAPSHOT_URL`指定发布jar包的maven仓库地址。

设置变量`MVN_SNAPSHOT_USERNAME`和`MVN_RELEASE_USERNAME`指定maven仓库的用户名。

设置变量`MVN_SNAPSHOT_PASSWORD`和`MVN_RELEASE_PASSWORD`指定maven仓库的密码

你可以使用任何 **gradle** 支持的方式来设置这些变量，例如可以在你的项目的 `build.gradle` 文件同目录下创建一个 **gradle.properties** 文件，并在里面设置这些变量；或者可以在 **~/.gradle** 目录下的 **gradle.properties** 文件中设置这些变量。
